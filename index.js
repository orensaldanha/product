require('dotenv').config()
const client = require('./routes/appRoutes')
const api = require('./routes/apiRoutes')
const subdomain = require('express-subdomain');
const cors = require('cors')
const path = require('path')

//Set the Path
process.env.APPPATH = __dirname

const express = require("express")
const app = express()



app.use(cors())

app.use(subdomain('api', api));

//Handle App Routes
app.use(express.static(path.join(__dirname, 'client/dist')));
app.use(subdomain('app', client));

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname,'/client/dist/error.html'));
})


app.listen(3000, function() {
    console.log("Server running on port 3000")
})

