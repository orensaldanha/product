const path = require("path")
const express = require('express');
const appRouter = express.Router();


appRouter.get('/*', function(req, res) {
     res.sendFile(path.join(process.env.APPPATH+'/client/dist/client.html'));
    
});


module.exports = appRouter
