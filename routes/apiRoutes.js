const express = require('express');
const apiRouter = express.Router();
const nonce = require('nonce')();
const cookie = require('cookie');
const querystring = require('querystring');
const crypto = require('crypto');
const request = require('request-promise');
var ObjectId = require('mongodb').ObjectID;
const MongoClient = require('mongodb').MongoClient;
const axios = require("axios")

let db
let product


const dbUrl = process.env.DB_URL;
const dbName = process.env.DB_NAME;

const apiKey = process.env.SHOPIFY_API_KEY;
const apiSecret = process.env.SHOPIFY_API_SECRET;
const scopes = 'read_products';
const forwardingAddress = process.env.SHOPIFY_REDIRECT_URL;

MongoClient.connect(dbUrl)
.then(connection => {
  console.log("Connected successfully to server");
   db = connection.db(dbName);
   user = db.collection('product');
}).catch(error => {
  console.log('ERROR:', error);
});

// STORE THE SHOPIFY STORE

apiRouter.get('/shopify', function(req, res) {
    const shop = req.query.shop;
    const userid = req.query.userid;
    user.update({ "_id" : ObjectId(userid)}, {$set:{"shop": shop}})

    if (shop) {
      const state = nonce();
      const redirectUri = forwardingAddress ;
      const installUrl = 'https://' + shop +
        '/admin/oauth/authorize?client_id=' + apiKey +
        '&scope=' + scopes +
        '&state=' + state +
        '&redirect_uri=' + redirectUri;
  
      res.cookie('state', state);
      res.redirect(installUrl);
    } else {
      return res.status(400).send('Missing shop parameter. Please add ?shop=your-development-shop.myshopify.com to your request');
    }
});

apiRouter.get('/shopify/test', (req, res) => {
    const shopRequestUrl = 'https://orenteststore.myshopify.com/admin/shop.json';
    const shopRequestHeaders = {
      'X-Shopify-Access-Token': "a51dd3035bd0d6538f280931cda5a0fb",
    };

    axios.get(shopRequestUrl, { headers: { "X-Shopify-Access-Token": "a51dd3035bd0d6538f280931cda5a0fb" } })
    .then(shop => {
        res.send(shop.data);
    })
});

apiRouter.get('/shopify/callback', (req, res) => {
    const { shop, hmac, code, state } = req.query;
    const stateCookie = cookie.parse(req.headers.cookie).state;
  
    if (state !== stateCookie) {
      return res.status(403).send('Request origin cannot be verified');
    }
  
    if (shop && hmac && code) {
      // DONE: Validate request is from Shopify
      const map = Object.assign({}, req.query);
      delete map['signature'];
      delete map['hmac'];
      const message = querystring.stringify(map);
      const providedHmac = Buffer.from(hmac, 'utf-8');
      const generatedHash = Buffer.from(
        crypto
          .createHmac('sha256', apiSecret)
          .update(message)
          .digest('hex'),
          'utf-8'
        );
      let hashEquals = false;
  
      try {
        hashEquals = crypto.timingSafeEqual(generatedHash, providedHmac)
      } catch (e) {
        hashEquals = false;
      };
  
      if (!hashEquals) {
        return res.status(400).send('HMAC validation failed');
      }
  
      const accessTokenRequestUrl = 'https://' + shop + '/admin/oauth/access_token';
      const accessTokenPayload = {
        client_id: apiKey,
        client_secret: apiSecret,
        code,
      };
  
      request.post(accessTokenRequestUrl, { json: accessTokenPayload })
      .then((accessTokenResponse) => {
        const accessToken = accessTokenResponse.access_token;
        user.update({ "shop" : shop}, {$set:{"shopifyaccess": accessToken, "newUser":0}})
        console.log(accessToken)
        res.redirect('http://app.lvh.me:8080/welcome-back?newuser=false');
      })
      .catch((error) => {
        res.status(error.statusCode).send(error.error.error_description);
      });
  
    } else {
      res.status(400).send('Required parameters missing');
    }
  });
  

apiRouter.get('/user/newuser', function(req, res) {
    const userid = req.query.userid
    user.findOne({ "_id" : ObjectId(userid) })
    .then(user => {
        res.json({newUser: user.newUser});
    });
});


apiRouter.get('/*', function(req, res) {
    res.send('Welcome to our API!');
});
 


module.exports = apiRouter

/*
 // DONE: Use access token to make API call to 'shop' endpoint
        const shopRequestUrl = 'https://' + shop + '/admin/shop.json';
        const shopRequestHeaders = {
          'X-Shopify-Access-Token': accessToken,
        };
  
        request.get(shopRequestUrl, { headers: shopRequestHeaders })
        .then((shopResponse) => {
          res.status(200).end(shopResponse);
        })
        .catch((error) => {
          res.status(error.statusCode).send(error.error.error_description);
        });
        */