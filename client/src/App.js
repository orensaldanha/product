import React, { Component } from 'react';
import { Navbar, Button } from 'react-bootstrap';
import AppRoutes from "./Components/AppRoutes/AppRoutes"
import Sidebar from "./Components/Sidebar/Sidebar"
import NewUser from "./Components/NewUser/NewUser"
import axios from "axios"
import history from './history';
import './App.css';

// Change the auth0 variables and shopify welcome back user

class App extends Component {

  login() {
    this.props.auth.login();
  }

  getProfile() {
    this.props.auth.getProfile();
  }



  render() {
    const { isAuthenticated } = this.props.auth; 

   return (
     <div>
       {
         !isAuthenticated()  && (
          <Button
          bsStyle="primary"
          className="btn-margin"
          onClick={this.login.bind(this)}
        >
          Login
        </Button>
         
        )
       }
       
       { 
         isAuthenticated()  && (
           
          <div className="container-fluid"> 
            <div className="row">
            <Sidebar auth={this.props.auth}/>
            <AppRoutes/>
            </div>
          </div>
        )
       }
  
       
     </div>
  );
  }
}

export default App;

