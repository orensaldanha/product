import React from 'react';
import { Route, Router } from 'react-router-dom';
import App from './App';
import Callback from './Components/Auth/Callback';
import WelcomeBack from  './Components/NewUser/WelcomeBack'
import Auth from './Components/Auth/Auth';
import Error from './Components/Error/Error.js'
import history from './history';
import NewUser from './Components/NewUser/NewUser';


const auth = new Auth();

const handleAuthentication = ({location}) => {
  if (/access_token|id_token|error/.test(location.hash)) {
    auth.handleAuthentication();
  }
}

export const makeMainRoutes = () => {
  return (
      <Router history={history}>
        <div>
          <Route  path="/" render={(props) => <App auth={auth} {...props} />} />
          <Route  path="/welcome" render={() => <NewUser />}  />
          <Route  path="/welcome-back" render={() => <WelcomeBack />}  />
          <Route  path="/error" render={() => <Error />}  />
          <Route path="/callback" render={(props) => {
            handleAuthentication(props);
            return <Callback {...props} /> 
          }}/>
        </div>
      </Router>
  );
}
