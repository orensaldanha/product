import React, {Component} from "react"
import Icon from 'react-icons-kit';
import {home2} from 'react-icons-kit/icomoon/home2'
import {NavLink} from "react-router-dom"


class Sidebar extends Component {
    logout() {
        this.props.auth.logout();
      }

  render() {
      return (
          <div className="Sidebar col">
            <div className="Logo">
                <img src="/logo.png" className="img-fluid logo" alt="Logo"/>
            </div>
            <div className="navigation">
            <nav className="nav flex-column">
                <NavLink className="nav-link" exact={true} activeClassName='is-active' to='/'>
                    <span className="nav-icon">
                    <Icon icon={home2} />
                    </span>
                    Home
                </NavLink>
                <NavLink className="nav-link" exact={true} activeClassName='is-active' to='/about'>
                    <span className="nav-icon">
                    <Icon icon={home2} />
                    </span>
                    About
                </NavLink><NavLink className="nav-link" exact={true} activeClassName='is-active' to='/ggr'>
                    <span className="nav-icon">
                    <Icon icon={home2} />
                    </span>
                    Home
                </NavLink>
                <br />
                <NavLink className="nav-link" exact={true} activeClassName='is-active' to='/hgh'>
                    <span className="nav-icon">
                    <Icon icon={home2} />
                    </span>
                    Home
                </NavLink>
                <NavLink className="nav-link" exact={true} activeClassName='is-active' to='/gh'>
                    <span className="nav-icon">
                    <Icon icon={home2} />
                    </span>
                    Home
                </NavLink><NavLink className="nav-link" exact={true} activeClassName='is-active' to='/ggr'>
                    <span className="nav-icon">
                    <Icon icon={home2} />
                    </span>
                    Home
                </NavLink>
                <br />
                <NavLink className="nav-link" exact={true} activeClassName='is-active' to='/fhh'>
                    <span className="nav-icon">
                    <Icon icon={home2} />
                    </span>
                    Home
                </NavLink>
                <NavLink className="nav-link" exact={true} activeClassName='is-active' to='/gh'>
                    <span className="nav-icon">
                    <Icon icon={home2} />
                    </span>
                    Home
                </NavLink><NavLink className="nav-link" exact={true} activeClassName='is-active' to='/ggr'>
                    <span className="nav-icon">
                    <Icon icon={home2} />
                    </span>
                    Home
                </NavLink>
                <br />
                <a className="nav-link logout-link"  href="/" onClick={this.logout.bind(this)}>
                    <span className="nav-icon">
                    <Icon icon={home2} />
                    </span>
                    Logout
                </a>
            </nav>
            </div>
         </div>
      )
  }
}

export default Sidebar