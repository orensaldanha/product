import React, {Component} from "react"
import {Route} from "react-router-dom"
import Home from "./Home"
import About from "./About"

class AppRoutes extends Component {
    render() {
        return(
          <div className="col-10 AppRoutes scroll">
            <Route exact path="/" component={Home}/>
            <Route path="/about" component={About}/>
          </div>      
        )
    }
}

export default AppRoutes