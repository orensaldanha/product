import React, { Component } from "react"
import axios from "axios"

class Home extends Component {
    componentDidMount() {
     
        axios.get("http://api.lvh.me:3000/shopify/test")
        .then(res => {
            console.log(res.data);
        })
    }

    render() {
        return (
            <div>
                This is the Home
            </div>
        )
    }
}

export default Home