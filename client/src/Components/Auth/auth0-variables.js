export const AUTH_CONFIG = {
  domain: process.env.domain,
  clientId: process.env.clientId,
  callbackUrl: process.env.callbackUrl
}
