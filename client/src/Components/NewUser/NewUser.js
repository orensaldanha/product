import React, { Component } from 'react'

 class NewUser extends Component {
    
  render() {
    const style = {
        position: 'absolute',
        display: 'flex',
        justifyContent: 'center',
        height: '100vh',
        width: '100vw',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: '#e2edde',
      }
     
      const user_id = localStorage.getItem('user_id')

    return (
      <div style={style}>
        <div>
            <h1 className="text-justify">CONNECT TO SHOPIFY</h1>
            <img src="http://pluspng.com/img-png/shopify-logo-png-file-shopify-logo-png-700.png" height="150px" width="500px"/>

            <form method="GET" action="http://api.lvh.me:3000/shopify">
                <input type="hidden" name="userid" value={user_id} />
                <input type="text" name="shop" id="shop" placeholder="example.myshopify.com"/>
                <button type="submit">Install</button>
            </form>
        
        </div>
      </div>
    )
  }
}

export default NewUser

