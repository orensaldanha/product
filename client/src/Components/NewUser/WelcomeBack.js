import React, { Component } from 'react';
import { Redirect } from "react-router"
import history from "../../history"

class WelcomeBack extends Component {
  componentDidMount() {
      localStorage.setItem('new_user', 0);
      history.replace('/')
  }
  render() {
    const style = {
      position: 'absolute',
      display: 'flex',
      justifyContent: 'center',
      height: '100vh',
      width: '100vw',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: 'white',
    }

    return (
      <div style={style}>
        <img src="/loading.svg" alt="loading" />
      </div>
    );
  }
}

export default WelcomeBack;