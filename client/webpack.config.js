const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Dotenv = require('dotenv-webpack');


module.exports = {
  entry: './src/index.js',
  output: {
    path: path.join(__dirname, '/dist'),
    filename: 'client_bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/client.html',
      filename:'client.html'
    }),
    new Dotenv()
  ],
  devServer: {
    contentBase: './dist',
    hot: true,
    disableHostCheck: true,
    historyApiFallback: {
      rewrites : [
        {from: /.*/, to: '/client.html'}
      ]
    }
  }
};
